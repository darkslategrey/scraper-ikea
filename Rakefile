# frozen_string_literal: true

# $LOAD_PATH.unshift('lib')
require 'pry'
require 'rake'
require 'hanami/rake_tasks'
require 'rake/testtask'
require 'github_changelog_generator/task'

require 'mongo'
require 'dotenv'

Dotenv.load

Rake::TestTask.new do |t|
  t.pattern = 'spec/**/*_spec.rb'
  t.libs << 'spec'
  t.warning = false
end

FileList['lib/tasks/**/*.rake', 'sources/**/*.rake'].each do |task|
  load task
end

GitHubChangelogGenerator::RakeTask.new :changelog do |config|
  config.since_tag = '0.1.14'
  config.future_release = '0.2.0'
end

desc 'chouard_org: go scrap chouard_org'
task :chouard do
  sh './bin/cli.rb scrap chouard_org'
end

namespace :status do
  desc 'status:sk gives sk status'
  task :sk do
    if File.exist?('tmp/sidekiq.pid')
      puts 'on'
    else
      puts 'off'
    end
  end
end

namespace :reset do
  desc 'db: reset db'
  task :db do
    client = Mongo::Client.new(ENV['MONGODB_URI'])
    db     = client.database
    db.drop
  end

  task :sk do
    cmd = 'echo flushall | redis-cli'
    sh cmd
  end

  task :log do
    IO.write('logs/sidekiq.log', '')
  end

  task :html do
    FileUtils.rm_r ENV['HTMLFILES_DIR']
  end

  task :all do
    [:log, :db, :sk, :html].each { |t| Rake::Task["reset:#{t}"].invoke }
  end
end

desc 'logs: tail f logs/sidekiq.log'
task :log do
  sh 'tail -f logs/sidekiq.log'
end

desc 'test cli'
task :ikea_test do
  sh 'ruby -r ./config/environment.rb -Ispec -Ilib:test spec/ikea/ikea_spec.rb'
end

task default: :test
task spec: :test
