# frozen_string_literal: true

source 'https://rubygems.org'

gem 'hanami', '~> 1.1'
gem 'hanami-model', '~> 1.1'
gem 'rake'

gem 'sqlite3'

group :development do
  # Code reloading
  # See: http://hanamirb.org/guides/projects/code-reloading
  gem 'shotgun'
end

group :test, :development do
  gem 'capybara'
  gem 'dotenv', '~> 2.0'
  gem 'factory_bot'
  gem 'minitest'
  gem 'minitest-skip'
  gem 'minitest-reporters'
  gem 'minitest-stub_any_instance'
  gem 'pry'
  #  gem 'pry-byebug'
  gem 'timecop'
  gem 'vcr'
  gem 'webmock'
  gem 'whenever-test'
  gem 'rubocop', require: false
end

group :production do
  gem 'puma'
end

gem 'redis'    # sidekiq backend / also use for timestamps
gem 'sinatra'  # sidekiq monitoring web app and front
gem 'mongoid'  # mongo orm to access main backend db
gem 'ice_cube' # manage scheduled timestamps
gem 'sidekiq'  # async queue task system

gem 'excon' # http client
gem 'faraday' # http client framework
gem 'faraday-conductivity'
gem 'faraday-cookie_jar' # http middlewares
gem 'faraday_middleware'
gem 'net-http-persistent' # alternative http client

gem 'couchrest' # simple couchdb client
gem 'json'
gem 'logging'
gem 'nokogiri' # html parsing
gem 'whenever', require: false # generate crontabs

gem 'foreman'   # process manager (for workers)

gem 'kiba'      # ETL tool. Extracted format => Mysql
# gem 'mongodb'   # simple mongo ORM
gem 'mongo'

gem 'bson_ext' # json perf 
gem 'capistrano' # deploy tool
gem 'github_changelog_generator'
gem 'activemodel'
gem 'activerecord'
gem 'mysql2'

gem "rb-readline", "~> 0.5.5"

gem "rdf-microdata", "~> 2.2"

gem "linkeddata", "~> 3.0"
