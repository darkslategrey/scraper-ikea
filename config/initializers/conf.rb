# frozen_string_literal: true

module Scraper
  class Main
    include Scraper::BLogger
    extend Dry::Configurable

    setting :sources
  end
end

# load sources/*.yml files
Scraper::Main.configure do |config|
  yamls = Scraper::Utils.new.load_yamls
  config.sources = Scraper::Utils.new.open_struct yamls
end
