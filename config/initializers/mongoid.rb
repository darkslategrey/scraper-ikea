Mongoid.load!(File.join(__dir__, "../mongoid.yml"), ENV["HANAMI_ENV"])
Mongoid::QueryCache.enabled   = false
Mongoid.raise_not_found_error = false
Mongoid.log_level = :error
