
# frozen_string_literal: true

ENV['HTMLFILES_DIR'] ||= "#{Dir.pwd}/htmlfiles"
htmlfiles_dir = ENV['HTMLFILES_DIR']

File.exist?(htmlfiles_dir) ||
  (Dir.mkdir(htmlfiles_dir) && puts("<#{htmlfiles_dir}> created"))

["#{Dir.pwd}/logs", "#{Dir.pwd}/tmp"].each do |dir|
  (!File.exist? dir) && Dir.mkdir(dir) && puts("<#{dir}> created")
end
