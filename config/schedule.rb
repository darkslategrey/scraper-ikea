# frozen_string_literal: true

# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

set :env_path, '"$HOME/.rbenv/shims":"$HOME/.rbenv/bin"'
set :environment, ENV['RACK_ENV']
set :output, './logs/cron.log'

job_type :rake, 'cd :path && PATH=:env_path:"$PATH" RAILS_ENV=:environment bundle exec rake :task --silent :output'

# every 5.minutes do
#   rake 'app:run'
# end

# Learn more: http://github.com/javan/whenever
