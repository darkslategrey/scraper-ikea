set :rails_env, 'production'
set :host, 'stg.courseur.com'
set :user, 'greg'

set :branch, ENV['BRANCH'] || 'develop'

role :app, %w{greg@stg.courseur.com}
role :web, %w{greg@stg.courseur.com}
role :db,  %w{greg@stg.courseur.com}

set :rbenv_ruby, '2.4.2'
