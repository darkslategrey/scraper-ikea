#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../config/boot'

module Scraper
  module CLI
    module Commands
      extend Hanami::CLI::Registry

      class List < Hanami::CLI::Command
        desc 'list sources'
        def call(*)
          puts ::Scraper::Main.config.sources.map(&:name).join("\n")
        end
      end

      class Scrap < Hanami::CLI::Command
        include Scraper::BLogger

        argument :source, required: true, desc: 'the source to scrap'
        desc 'scrap a source'
        def call(source:, **)
          conf_src = Scraper::Main.config.sources.select do |src|
            src.name == source.to_sym
          end.first
          conf_src = conf_src.loaded
          inputs   = Scraper::Utils.new.load_inputs(conf_src).map(&:to_h)
          inputs   = Scraper::Utils.new.store_inputs(inputs)
          params   = {
            source: Scraper::Utils.new.os_to_h(conf_src),
            inputs: inputs
          }
          ::Scraper::MainWorker.perform_async(params)
        rescue StandardError => e
          msg = "not found <#{source}>: <#{e.message}> "
          msg += "<#{e.backtrace.join("\n")}>"
          log.error msg
          raise e
        end
      end

      # {"parser":"Product",
      # "outfile":"htmlfiles3/product_5fd242ecaf6571491c6310938f2ab3e2.html"}
      class Parse < Hanami::CLI::Command
        argument :parser, required: true,
                 desc: 'Parser to run: [categories|category|product]'
        argument :htmlfile,
                 desg: 'Html file to parse. Default: the first one'

        desc 'parse an html file'
        def call(parser:, htmlfile: nil, **)
          firsthtml = Dir["#{ENV['HTMLFILES_DIR']}/#{parser}**.html"].first
          htmlfile  = htmlfile.blank? ? firsthtml : htmlfile
          (!File.exist? htmlfile) && abort("<#{htmlfile}> does not exist. Existing !")

          params = {
            parser: parser.capitalize,
            outfile: htmlfile
          }
          puts params
          Redis.new.publish('parse', params.to_json)
        end
      end

      module Generate
        class Base < Hanami::CLI::Command
          def generate_script(name, parser)
            exposures = Hash[name: name, parser: parser]
            template_file = 'lib/templates/source_fr/parser_1.rb.erb'
            template  = Hanami::View::Template.new(template_file)
            source_view = Scraper::Generate::Source.new(template, exposures)
            FileUtils.mkdir_p("sources/#{name}")
            IO.write("sources/#{name}/#{parser}.rb", source_view.render)
          end
        end
        class Script < Base
          argument :script, require: true, desc: 'script source:script_name (e.g. google_fr:search'

          desc 'generate a source conf'
          def call(script:, **)
            source, parser = script.split(':')
            generate_script(source, parser)
          end
        end
        class Source < Base
          argument :name, require: true, desc: 'source name (e.g. google_fr)'
          argument :parser, require: true, desc: 'source name (e.g. blog)'

          desc 'generate a source conf'
          def call(name:, parser:, **)
            generate_source(name, parser)
            generate_script(name, parser)
            generate_inputs(name)
            generate_spec(name, parser)
          end

          def generate_spec(name, parser)
            FileUtils.mkdir_p("sources/#{name}")
            exposures = Hash[name: name, parser: parser]
            template_file = 'lib/templates/source_fr_spec.rb.erb'
            template  = Hanami::View::Template.new(template_file)
            source_view = Scraper::Generate::Source.new(template, exposures)
            IO.write("spec/sources/#{name}_spec.rb", source_view.render)
          end

          def generate_inputs(name)
            FileUtils.cp('lib/templates/source_fr/inputs.csv',
                         "sources/#{name}")
          end

          def generate_source(name, parser)

            FileUtils.mkdir_p("spec/fixtures/#{name}")

            exposures = Hash[name: name, parser: parser]
            template_file = 'lib/templates/source_fr.yml.erb'
            template  = Hanami::View::Template.new(template_file)
            source_view = Scraper::Generate::Source.new(template, exposures)
            IO.write("sources/#{name}.yml", source_view.render)
          end
        end
      end
      register 'parse', ::Scraper::CLI::Commands::Parse
      register 'scrap', ::Scraper::CLI::Commands::Scrap
      register 'ls',    ::Scraper::CLI::Commands::List
      register 'generate source', ::Scraper::CLI::Commands::Generate::Source
      register 'generate script',
               ::Scraper::CLI::Commands::Generate::Script
    end
  end
end
Hanami::CLI.new(Scraper::CLI::Commands).call
