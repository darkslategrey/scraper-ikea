<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Scraper](#scraper)
    - [Requirements](#requirements)
    - [Usage](#usage)
        - [Install dependencies:](#install-dependencies)
        - [Start services](#start-services)
        - [Scraper usage](#scraper-usage)
            - [Logs](#logs)
        - [Limits](#limits)
        - [Env VARS](#env-vars)
    - [Tech notes](#tech-notes)
        - [Rake tasks](#rake-tasks)
        - [Tests](#tests)
        - [Achitecture pitch](#achitecture-pitch)
        - [Mongodb data format](#mongodb-data-format)
            - [Categories](#categories)
            - [Category](#category)
            - [Product](#product)
            - [Availability](#availability)
        - [Customize data extraction](#customize-data-extraction)

<!-- markdown-toc end -->
# Scraper

This project aims to scrape the ikea catalog from 'http://www.ikea.fr'.

By now it is possible to scrape :

- `categories`, this is the entry point. The html doc contains all category's
  url;
- `category`, each html doc contains all product's urls of one category;
- `product`, html doc contain info about the product itself and urls to its
  availability;
- `availability` xml file containing store stocks.

Everything will be saved into mongodb (see .env* files)

A mini etl starter is present into `sources/ikea_fr/etl/*`.
It allow to transform data from mongodb to an other format/db.

## Requirements

- Redis server
- Mongodb server

## Usage

### Install dependencies:

```shell
$ bundle
```

### Start services

This will start sidekiq and the sidekiq dashboard available at
<http://localhost:2300/sk>.

```shell
$ rake start:all
```

### Scraper usage

```shell
./bin/cli.rb help
Commands:
  cli.rb ls                                      # list sources
  cli.rb parse PARSER [HTMLFILE]                 # parse an html file
  cli.rb scrap SOURCE                            # scrap a source
```

#### Logs

`logs/sidekiq.log`
`logs/scraper.log`

### Limits

`IKEA_CAT_LIMIT and IKEA_PROD_LIMIT` are two env vars you can use to limit
downloads of category and product respectively.

### Env VARS

* `MYSQL_URI`
* `MONGODB_URI`
* `HTMLFILES_DIR`   where to store downloaded html files
* `IKEA_CAT_LIMIT`  limit categories downloads
* `IKEA_PROD_LIMIT` limit products downloads
* `IKEA_AVAIL_LIMIT` limit availability downloads

## Tech notes

### Rake tasks

```shell
rake changelog        # Generate a Change log from GitHub
rake environment      # Load the full project
rake reset            # restart: stop, start, tail -f on sidekiq.log
rake restart:all      # RESTART all
rake restart:sidekiq  # RESTART sidekiq
rake start:all        # START sidekiq and sk_front
rake start:etl        # START etl mongo to mysql
rake start:sidekiq    # START sidekiq
rake start:sk_front   # START sidekiq front
rake stop:all         # STOP sidekiq and sk_front
rake stop:sidekiq     # STOP sidekiq
rake stop:sk_front    # STOP sidekiq front
rake test             # Run tests
```

### Tests

`rake`

### Achitecture pitch

TODO

### Mongodb data format

#### Categories

From the `outfile` the `categories` ruby script extracted 2 `urls.category`.
We compute the corresponding id stored in the `ids.category` item.
These ids represents mongo category `_id's`.

```javascript
{
    "_id" : "50c8c2ced7edef1869094b125891d0c4",
    "urls" : {
        "category" : [ 
            "/fr/fr/catalog/categories/business/"
        ]
    },
    "data" : [],
    "outfile" : "htmlfiles/ikea_fr/categories_50c8c2ced7edef1869094b125891d0c4.html",
    "ids" : {
        "category" : [ 
            "cdd8ce9745b27e89fee2b4843ea69d37"
        ]
    }
}
```

#### Category

Note the `_id`. Here the `category` ruby script extract two products url's. 
And some other data.

```javascript
{
    "_id" : "cdd8ce9745b27e89fee2b4843ea69d37",
    "urls" : {
        "product" : [ 
            "/fr/fr/catalog/products/10358664/", 
            "/fr/fr/catalog/products/80351188/"
        ]
    },
    "data" : [ 
        {
            "main_title" : "CHAMBRE ENFANTS"
        }, 
        {
            "sub_title" : "Pour les personnes les plus importantes au mondeTOUS LES DEPARTEMENTS:\n                    \n                             \n                                Les catalogues IKEA                                                \n                            \n                        \n                    Voici IKEAIKEA FAMILYConcevoir son intérieurLes services IKEAInformations pratiques et services"
        }, 
        {
            "desc" : "Découvrez des systèmes de rangement modulables adaptés aux besoins de votre enfant selon son âge, des ensembles complets pour une chambre harmonieuse, des rangements simples pour ranger jouets, jeux et petites affaires... Un univers bien à eux répondant à la fois aux besoins des enfants et parents."
        }
    ],
    "outfile" : "htmlfiles/ikea_fr/category_cdd8ce9745b27e89fee2b4843ea69d37.html",
    "ids" : {
        "product" : [ 
            "2899cab8be6b19406caea9e352754652", 
            "fd27df156fe64f234317322522ffa4b1"
        ]
    }
}
```

#### Product

The product ruby extract one `availability` url.

```javascript
{
    "_id" : "2899cab8be6b19406caea9e352754652",
    "urls" : {
        "availability" : [ 
            "/fr/fr/iows/catalog/availability/10358664/"
        ]
    },
    "data" : {
        "items" : [ 
            {
                "custMaterials" : "100% coton<br/>",
                "descriptiveAttributes" : {},
                "imperial" : "Nombre de fils /cm²: 144 pouce carré<br/>Longueur housse couette: 79 \"<br/>Largeur housse couette: 59 \"<br/>Long. taie oreiller: 26 \"<br/>Larg. taie oreiller: 26 \"<br/><br/>",
                "catEntryId" : "1077405",
                "dualCurrencies" : false,
                "pkgInfoArr" : [ 
                    {
                        "articleNumber" : "10358664",
                        "pkgInfo" : [ 
                            {
                                "weightMet" : "0.91 kg",
                                "widthMet" : "19 cm",
                                "quantity" : "1",
                                "consumerPackNo" : 1,
                                "lengthMet" : "28 cm",
                                "heightMet" : "5 cm"
                            }
                        ]
                    }
                ],
                "type" : "Housse de couette et taie",
                "reqAssembly" : false,
                "soldSeparately" : "",
                "name" : "STILLSAMT",
                "bti" : false,
                "attachments" : [],
                "partNumber" : "10358664",
                "avg_rating" : "5",
                "validDesign" : [ 
                    "orange clair"
                ],
                "goodToKnowPIP" : "Nombre de fils : 57/cm².<br/>La valeur du nombre de fils indique le nombre de fils par centimètre carré de tissu. Plus le nombre de fils est élevé, plus le tissage sera dense et résistant.<br/>",
                "techInfoArr" : [],
                "careInst" : "Lavage en machine, 60°C maximum, traitement normal.<br/>Pas de chlorage.<br/>Séchage en sèche-linge à tambour rotatif possible, température normale (80°C max).<br/>Repassage à une température maximale de la semelle du fer à repasser de 200 °C.<br/>Pas de nettoyage à sec.<br/>",
                "extendedContext" : [ 
                    {
                        "articleNumber" : "10358664",
                        "extendedContext" : [ 
                            {
                                "avg_rating" : "5",
                                "rating_count" : "1",
                                "gprAvgRating" : "5",
                                "gprRatingCount" : "1"
                            }
                        ]
                    }
                ],
                "designer" : "IKEA of Sweden",
                "nopackages" : "1",
                "goodToKnow" : "Nombre de fils : 57/cm².<br/>La valeur du nombre de fils indique le nombre de fils par centimètre carré de tissu. Plus le nombre de fils est élevé, plus le tissage sera dense et résistant.<br/>",
                "url" : "/fr/fr/catalog/products/10358664/",
                "packagePopupUrl" : "/fr/fr/catalog/packagepopup/10358664/",
                "availabilityUrl" : "/fr/fr/catalog/availability/10358664/",
                "environment" : "Matériau renouvelable (coton).<br/>Tout le coton de nos produits est issu de sources plus durables. Cela signifie que le coton est soit recyclé, soit cultivé selon des méthodes utilisant moins d’eau, d&#39;engrais et de pesticides, tout en augmentant les revenus des cultivateurs.<br/>Aucun agent de blanchiment optique n&#39;a été utilisé.<br/>Blanchi sans chlore.<br/>",
                "rating_count" : "1",
                "color" : "orange clair",
                "custBenefit" : "<cbs><cb><t>En coton, doux et agréable sur la peau de votre enfant.</t></cb><cb><t>Tissé serré, ce tissu 100% coton permet au linge de lit de ne pas se décolorer et de devenir plus doux et plus résistant à chaque lavage.</t></cb><cb><t>Facile à entretenir ; lavage en machine à haute température(60°C).</t></cb></cbs>",
                "metric" : "Nombre de fils /cm²: 144 pouce carré<br/>Longueur housse couette: 200 cm<br/>Largeur housse couette: 150 cm<br/>Long. taie oreiller: 65 cm<br/>Larg. taie oreiller: 65 cm<br/><br/>",
                "buyable" : true,
                "prices" : {
                    "comparisonPriceExists" : false,
                    "hasTemporaryFamilyOffer" : false,
                    "hasFamilyPrice" : false,
                    "usesUnitPriceMeasure" : false,
                    "isUnitPricePrimary" : false,
                    "normal" : {
                        "priceNormal" : {
                            "priceExclVat" : "12,49 €",
                            "value" : "14,99 €",
                            "rawPrice" : 14.99
                        },
                        "priceNormalDual" : {},
                        "priceNormalPerUnit" : {
                            "unit" : ""
                        }
                    },
                    "hasEcoFee" : false,
                    "enablenlpinterval" : 0,
                    "hasPrfCharge" : false
                },
                "images" : {
                    "zoom" : [ 
                        "/PIAimages/0481770_PE619801_S5.JPG", 
                        "/PIAimages/0481769_PE619802_S5.JPG", 
                        "/PIAimages/0499830_PE630669_S5.JPG"
                    ],
                    "normal" : [ 
                        "/PIAimages/0481770_PE619801_S3.JPG", 
                        "/PIAimages/0481769_PE619802_S3.JPG", 
                        "/PIAimages/0499830_PE630669_S3.JPG"
                    ],
                    "thumb" : [ 
                        "/PIAimages/0481770_PE619801_S2.JPG"
                    ],
                    "small" : [ 
                        "/PIAimages/0481770_PE619801_S1.JPG"
                    ],
                    "large" : [ 
                        "/fr/fr/images/products/stillsamt-housse-de-couette-et-taie-orange__0481770_PE619801_S4.JPG", 
                        "/fr/fr/images/products/stillsamt-housse-de-couette-et-taie-orange__0481769_PE619802_S4.JPG", 
                        "/fr/fr/images/products/stillsamt-housse-de-couette-et-taie-orange__0499830_PE630669_S4.JPG"
                    ]
                },
                "californiaTitle20Product" : false
            }
        ],
        "catEntryId" : "1075971",
        "attributes" : [ 
            {
                "id" : "1001593364",
                "name" : "couleur",
                "type" : "00018"
            }, 
            {
                "id" : "1001593363",
                "name" : "dimensions",
                "type" : "00019"
            }
        ],
        "partNumber" : "37370 [GENERICPRODUCT]",
        "categories" : [ 
            "Bébé et enfant ", 
            "Textiles enfant", 
            "Linge de lit"
        ]
    },
    "outfile" : "htmlfiles/ikea_fr/product_2899cab8be6b19406caea9e352754652.html",
    "ids" : {
        "availability" : [ 
            "bfc51f92d6271efaa91a1c30ae0d9aab"
        ]
    }
}
```

#### Availability

<./availibilty.json>


### Customize data extraction

See `sources/ikea_fr/*.rb`

