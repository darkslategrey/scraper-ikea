module Scraper
  class Input
    include Mongoid::Document
    include Mongoid::Attributes::Dynamic
    include Mongoid::Timestamps::Short
    include ::Scraper::BLogger

    def encode_id
    end
    
    def add_data(data, parser)
      self[parser] ||= []
      self.push(parser => data)
      save!
    end
  end
end
