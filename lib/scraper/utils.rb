# frozen_string_literal: true

module Scraper
  # utilities
  class Utils
    include Scraper::BLogger

    def md5(s, src_id)
      Digest::MD5.new.update(s.to_s + src_id.to_s).hexdigest
    end

    # TODO: move inputs methods into input entities ?
    def store_inputs(coll)
      new_coll = []

      coll.each do |input|
        src_id = input[:src_id]
        input[:src_id] = src_id
        scraper_input  = Input.new(input)
        scraper_input.save!
        input[:_id] = scraper_input.id.to_s
        new_coll << input
      end
      log.debug new_coll
      new_coll # ids.map(&:to_s)
    end

    def load_inputs(params)
      csv = params[:inputs]
      return [] if csv.blank?

      csv     = IO.read(csv).split("\n")
      header  = csv.shift.split(';').map       { |i| i.strip }
      data    = csv.map { |d| d.split(';').map { |i| i.strip } }

      inputs  = []
      data.each do |item|
        input = CSVInput.new(csv_data: Hash[(0..header.size-1).map { |i|
                                              [header[i], item[i]]
                                            }])
        id = input.csv_data.delete 'id'
        input.src_id = id
        inputs << input
      end
      inputs
    end

    def build_erb_param(erb_param, input)
      matched = erb_param.scan(/<%= ([a-z_1-9]+) %>/).flatten
      return erb_param if matched.size == 0

      bind = binding
      matched.each { |m|
        url_param_val = input.send(m.to_sym)
        bind.eval("#{m.to_sym} = '#{url_param_val}'")
      }
      ERB.new(erb_param).result(bind)
    end

    def conf_by(name)
      Scraper::Main.config.sources.select do |src|
        src.name == name
      end.first
    end

    def make_filename(parser, id, msg, src_id)
      outformat = msg[:_format] || 'html'
      dir = "#{ENV['HTMLFILES_DIR']}/#{msg[:source].name}/#{src_id}"
      begin
        FileUtils.mkdir_p dir
      rescue StandardError
        log.debug("<#{dir}> exists!")
      end
      "#{dir}/#{parser}_#{id}.#{outformat}"
    end

    def default_ts
      rand((1..60)).send(:seconds).from_now
    end

    def load_yamls
      srcs = []
      dir = File.join(File.dirname(__FILE__), '..', '..', 'sources', '*.yml')
      Dir[dir].each do |yaml|
        log.debug "loading <#{yaml}>"
        loaded = YAML.safe_load(IO.read(yaml), [Symbol], [], true)
        name   = loaded[:name].to_sym
        srcs << { name: name, loaded: loaded }
      end
      srcs
    end

    # OpenStruct to Hash recursively
    def os_to_h(os)
      new_obj = {}
      case os.class.to_s
      when 'OpenStruct'
        os.each_pair do |k, v|
          new_obj[k] = os_to_h(v)
        end
      when 'Array'
        new_obj = os.map { |item| os_to_h(item) }
      else
        new_obj = os
      end
      new_obj
    end

    def open_struct(obj)
      new_obj = obj
      case obj.class.to_s
      when /oid/i
        new_obj = obj.to_s
      when 'Array'
        new_obj = obj.map do |elt|
          open_struct elt
        end
      when /hash|bson::document/i
        obj.each do |key, value|
          new_obj[key] = open_struct value
        end
        new_obj = OpenStruct.new new_obj
      end
      new_obj
    end

    def build_context(parser, filename, source)
      klass = "Scraper::#{source.camelize}::#{parser.camelize}"
      log.debug "<#{klass}> creation!"
      log.debug "to parse #{filename}"
      [Object.const_get(klass), parse_document(filename)]
    end

    def parse_document(filename)
      content = IO.read(filename)

      if filename.match?(/html$/)
        Nokogiri::HTML(content)
      else
        Nokogiri::XML(content)
      end
    end

    # from:
    # htmlfiles/codeur_fr/projets_1e3a92d0411dea4bf1d80a45da9e8b75.html
    # extract: 1e3a92d0411dea4bf1d80a45da9e8b75
    def file_to_id(file)
      file.split('/')[-1].split('_')[1].split('.').first
    end

    def urls_to_ids(parsers_urls = {}, input)
      msg = "urls <#{parsers_urls}> input id <#{input.src_id}>"
      log.debug(msg)
      ids = {}
      parsers_urls.each do |parser, urls|
        ids[parser] = urls.uniq.map do |url|
          { url: url, id: md5(url, input) }
        end
      end
      ids
    end
  end
end
