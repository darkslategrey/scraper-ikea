# frozen_string_literal: true

module Scraper
  # generate timestamps for sidekiq crawl jobs
  class TSGenerator
    def initialize(_options = {})
      @keyname = 'scraper:ts'
      @redis   = Redis.new
      @min     = 1
      @max     = 3
    end

    def generate
      @redis.del(@keyname)
      from = Time.now + 1.second
      to   = from + 8.hour

      timestamps = []
      current    = from

      while current < to
        current += rand(@min..@max).second
        timestamps << current.to_s
      end
      @redis.rpush @keyname, timestamps
    end

    def next
      generate if ts_empty? || ts_in_past?
      ts = @redis.lpop(@keyname)
      # generate if @redis.llen(@keyname) == 0 or ts.nil?

      # @redis.multi do |multi|
      #   ts = multi.lpop(@keyname)
      # end
      # ts = ts.value
      Time.parse(ts)
    ensure
      @redis.close
    end

    def ts_empty?
      @redis.llen(@keyname).zero?
    end

    def ts_in_past?
      Time.parse(@redis.lindex(@keyname, 0)) < Time.now
    end
  end
end
