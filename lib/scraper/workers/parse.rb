# frozen_string_literal: true

require_relative '../../../config/environment'
require_relative './store'
require_relative './scrap'

module Scraper
  # subscribe to redis 'parse' msg
  class ParseWorker
    include Sidekiq::Worker
    include Scraper::BLogger

    sidekiq_options retry: false, queue: 'parser'


    def perform(params)
      log.debug('start PARSE')
      do_parse(Utils.new.open_struct(params))
    rescue StandardError => e
      raise e
    end

    # params:
    # { outfile: outfile,
    #   parser: parser,
    #   url: url,
    #   input: Utils.new.os_to_h(input),
    #   source: Utils.new.os_to_h(source) }
    def do_parse(msg)
      parser = msg.parser
      url    = msg.url
      log.debug "Go parse <#{msg.outfile}> with <#{parser}> parser "
      log.debug msg.to_s
      parser_klass,
      nokodoc = Utils.new.build_context(parser, msg.outfile, msg.source.name)

      parsers_urls, data, format = parser_klass.new(nokodoc).run
      data ||= {}
      log.debug("data <#{data}>")
      log_msg = "urls <#{parsers_urls}> data.size <#{data.size}> "
      log_msg += "format <#{format}> input <#{msg.input}>"
      log.debug(log_msg)
      ids = do_store(parsers_urls, data, msg)
      urls = do_scrap(ids,
                      msg.source,
                      msg.input, format, url) \
        unless parsers_urls.blank?

      [parsers_urls, parser, format, urls]
    rescue StandardError => e
      log.error(e.to_s)
      log.error(e.backtrace[0..5].join("\n"))
      raise e
    end

    def do_store(parsers_urls, data, msg)
      parser  = msg.parser
      outfile = msg.outfile
      source  = msg.source
      data ||= []
      ids = Utils.new.urls_to_ids(parsers_urls, msg.input)
      store_msg = {
        parsers_urls: parsers_urls,
        ids: ids,
        outfile: outfile,
        parser: parser
      }
      store_msg[:data]  = data
      store_msg[:input] = Utils.new.os_to_h(msg.input)
      store_msg.merge!(Utils.new.os_to_h(source))

      Scraper::StoreWorker.perform_async store_msg
      ids
    rescue Exception => e
      log.error("cannot store: <#{msg}>: #{e.message}: <#{e.backtrace}>")
      raise e
    end

    def do_scrap(p_urls_ids, source, input, out_format='html', url='/')
      enqueued_urls   = []
      p_urls_ids.each do |parser, ids|
        log.debug "selected urls <#{ids}>"
        ids.each do |url_id|
          outfile = Utils.new.make_filename(parser, url_id[:id],
                                            { source: source,
                                              _format: out_format },
                                            input.src_id)
          logmsg  = "Outfile <#{outfile}> for parser <#{parser}> "
          logmsg += "url <#{url_id[:url]}>"
          log.debug logmsg
          params  = { outfile: outfile, parser: parser,
                      source: Utils.new.os_to_h(source),
                      url: url_id[:url],
                      input: Utils.new.os_to_h(input) }
          # Scraper::ScrapWorker.perform_in(Utils.new.default_ts, params) \
          #   if ENV['HANAMI_ENV'] != 'test'
          Scraper::ScrapWorker.perform_async(params) # \
          # if ENV['HANAMI_ENV'] == 'test'
          enqueued_urls << url_id[:url]
        end
      end
      enqueued_urls
    end
  end
end
