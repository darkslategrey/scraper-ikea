# frozen_string_literal: true

require_relative '../../../config/environment'
require_relative './parse'

# require_relative '../ts_generator'
# require 'sidekiq'

module Scraper
  class MainWorker # ScrapWorker
    include Sidekiq::Worker
    include Scraper::BLogger

    sidekiq_options retry: false, queue: 'main'

    def perform(params)
      log.debug('start Main')
      do_fetch(Utils.new.open_struct(params))
    rescue StandardError => e
      log.error(e.to_s)
    end

    def default_inputs
      default_id = Time.now.strftime('%d_%m_%y_%H_%M_%S')
      input      = Input.create({ src_id: default_id})
      input      = OpenStruct.new(input.attributes)
      input._id  = input._id.to_s
      [input]
    end

    # msg = {
    #   source: { this is the hash transcript of a sources/*.yml file },
    #   inputs: [collection of csv_inputs]
    # }
    def do_fetch(msg)
      inputs = msg.inputs.blank? ? default_inputs : msg.inputs
      inputs.each do |input|
        # from { parser_1: ['url1', 'url2'] }
        # to   { parser_1: [{url: 'url1', id: 123}, {url: 'url2', id: 345}]}
        from = msg.source.send(:start).urls.to_h
        with_ids = Utils.new.urls_to_ids(from, input)
        with_ids.each do |parser, urls_ids|
          h_source = Utils.new.os_to_h(msg.source)
          h_source[parser] = h_source[:start]
          # TODO: this duplicate with parser
          urls_ids.each do |url_id|
            outfile = Utils.new.make_filename(parser, url_id[:id],
                                              msg, input.src_id)
            params  = { outfile: outfile, parser: parser,
                        input: Utils.new.os_to_h(input),
                        source: h_source }
            log.debug outfile
            if !File.exist? outfile
              log.debug "scrap worker call for url <#{url_id[:url]}>"
              params[:url] = url_id[:url]
              # Scraper::ScrapWorker.perform_in(Utils.new.default_ts, params) \
              #   if ENV['HANAMI_ENV'] != 'test'
              Scraper::ScrapWorker.perform_async(params) # \
            # if ENV['HANAMI_ENV'] == 'test'
            else
              log.debug 'parse worker call'
              Scraper::ParseWorker.perform_async(params)
            end
          end
        end
      end
    rescue StandardError => e
      log.error(e.message)
      log.error(e.backtrace[0..5].join("\n"))
      raise e
    end
  end
end
