# frozen_string_literal: true

# require 'mongo/driver'
require 'mongo'
require_relative '../../../config/initializers/mongoid'
require_relative '../entities/input'

module Scraper
  class StoreWorker
    include Sidekiq::Worker
    include Scraper::BLogger

    @@client = Mongo::Client.new(ENV['MONGODB_URI'])
    @@db     = @@client.database

    sidekiq_options retry: false, queue: 'store'

    def perform(params)
      do_store(Utils.new.open_struct(params))
    rescue Exception => e
      raise e
    end

    def do_store(msg)
      urls    = msg.parsers_urls
      data    = msg.data
      outfile = msg.outfile
      input   = msg.input
      ids     = Utils.new.os_to_h(msg.ids)
      parser  = msg.parser

      store_raw(urls, data, outfile, ids, parser)
      store_input(input, data, parser)
    end

    def store_input(input, data, parser)
      ::Scraper::Input.find(input._id).add_data(data, parser)
      input.reload
      input
    rescue Exception => e
      log.error "cannod find input <#{input}>: <#{e.message}>"
      raise e
    end

    def store_raw(urls, data, outfile, ids, parser)
      id = Utils.new.file_to_id outfile
      value = { urls: urls, data: data, outfile: outfile, ids: ids }
      log.debug("Go persist for <#{id}> from <#{outfile}> ids: <#{ids}>")
      collection = @@db[parser.to_sym]
      result = collection.update_one({ '_id' => id },
                                     { '$set' => value },
                                     upsert: true)
      raise if result.n != 1
    rescue StandardError => e
      log.error(e.backtrace.join("\n"))
      raise e
    ensure
      @@client.close
    end
  end
end
