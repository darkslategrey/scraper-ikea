# frozen_string_literal: true

require_relative '../../../config/environment'
require_relative '../spider'

module Scraper
  class ScrapWorker
    include Sidekiq::Worker
    include ::Scraper::BLogger

    sidekiq_options retry: false, queue: 'scraper'

    def perform(params)
      url     = params['url']
      outfile = params['outfile']
      parser  = params['parser']
      source  = Utils.new.open_struct(params['source'])
      input   = params['input']
      source.input   = input
      log.debug("the parser <#{parser}>")
      source.current = source.send(parser.to_sym)
      log.debug("the parser <#{source.current}>")
      host = source.host
      log.debug(params.to_s)
      # use to specialize by parser http header / requests ...
      # source.current = source.send(parser.to_sym) # may be here ?
      log.debug("start performing <#{url}> for host <#{host}>")
      body = ::Scraper::Spider.new(source).go(url)
      log.debug("write to <#{outfile}> the body size <#{body.size}>")
      IO.write(outfile, body)
      parse_msg = { outfile: outfile, parser: parser, url: url,
                    input: Utils.new.os_to_h(input),
                    source: Utils.new.os_to_h(source) }
      Scraper::ParseWorker.perform_async parse_msg
    rescue ::Scraper::Spider::Error => e
      log.warn "http error <#{e.message}>"
      raise e
    end
  end
end
