# frozen_string_literal: true

module Scraper
  # add log method to classes
  module BLogger
    logging_pattern = "[%d] %-5l [%c]: %m\n"
    logging_layout = Logging.layouts.pattern(pattern: logging_pattern)
    logging_layout.date_pattern = '%d/%m/%y - %H:%M:%S'
    log_filename = File.expand_path('scraper{{.%d-%m-%Y}}.log', 'logs')
    rolling_file = Logging.appenders.rolling_file(log_filename,
                                                  layout: logging_layout,
                                                  age: 'daily',
                                                  keep: 1)
    root_logger           = Logging.logger.root
    root_logger.level     = ENV['HANAMI_ENV'] == 'test' ? :error : :debug
    root_logger.appenders = Logging.appenders.stdout(layout: logging_layout)
    root_logger.add_appenders rolling_file

    def self.included(receiver)
      # @@log = Logging.logger[receiver]
      receiver.extend(Scraper::BLogger)
    end

    def log
      Logging.logger[self]
    end
  end
end
