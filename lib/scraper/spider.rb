# frozen_string_literal: true

module Scraper
  # make the http request
  class Spider
    include Scraper::BLogger

    class Error < StandardError; end
    attr_reader :http_method, :headers, :host, :request, :current


    def initialize(params)
      log.debug "creation <#{params}>"
      # params   ||= 
      @current   = params.current || Utils.new.open_struct({})
      @current.request ||= {}
      @http_method = @current.http_method
      @http_method ||= params.http_method
      @http_method ||= :get
      @http_method = @http_method.to_sym
      @headers     = params.headers || {}
      @headers     = Utils.new.os_to_h(@headers)
      log.debug("headers <#{@headers}>")

      if params.host.blank?
        raise StandardError.new('please provide a host')
      end

      @host        = params.host
      @http_client = params.client || default_http
      @http_client.headers.merge! @headers

      if params.input and @current.request
        resolve_request(params.input["csv_data"],
                        Utils.new.os_to_h(@current.request))
      end
    rescue StandardError => e
      msg = "cannot create with params: <#{params}>: "
      msg += "<#{e.message}> <#{e.backtrace.join("\n")}>"
      log.error msg
      raise e
    end

    def resolve_request(input, request)
      @request = request
      binded_request = {}
      @request.each do |k, v|
        # TODO: strange ?!
        next if v.nil?
        csv_input         = CSVInput.new(input)
        binded_request[k] = Utils.new.build_erb_param(v, csv_input)
      end
      @request = binded_request
    end

    def excon_http
      Excon
    end

    def default_http
      Faraday.new(url: @host) do |faraday|
        faraday.request  :url_encoded
        faraday.request  :multipart

        faraday.use FaradayMiddleware::FollowRedirects, limit: 3
        faraday.response :logger if ENV['HANAMI_ENV'] == 'test'
        faraday.use :gzip
        faraday.use :repeater, retries: 3,
                    mode: :rapid
        faraday.use :cookie_jar
        faraday.adapter Faraday.default_adapter
        # faraday.adapter :excon
      end
    end

    def go(url, input=nil)
      log.debug "go fetching <#{url}> <#{@host}> <#{@http_method}>"
      url.gsub!(/^\//, ''); url.strip!
      log.debug "the request <#{@request}>"
      response = @http_client.send(@http_method,
                                   url,
                                   @request)
      log.debug "response status <#{response.status}> for <#{url}>"
      raise Error, response.status if response.status != 200
      response.body
    rescue StandardError => e
      log.error(e.to_s)
      raise e
    end
  end
end
