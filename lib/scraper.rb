# frozen_string_literal: true

require_relative 'scraper/logger'
require_relative 'scraper/utils'

require 'active_support/all'

require_relative 'scraper/workers/main'
require_relative 'scraper/entities/csv_input'

# require_relative 'scraper/spider'

# main module
module Scraper
end

dir = File.join(File.dirname(__FILE__), '..', 'sources', '**', '*.rb')

Dir[dir].each do |script|
  require_relative script.sub(/.rb$/, '')
end
