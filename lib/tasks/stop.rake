namespace :stop do
  desc 'STOP sidekiq and sk_front'
  task all: %i[sidekiq sk_front]

  desc 'STOP sidekiq front'
  task :sk_front do
    begin
      sh "kill $(ps aux | grep [p]uma | awk '{ print $2 }') 2>/dev/null"
    rescue StandardError
      ''
    end
  end

  desc 'STOP sidekiq'
  task :sidekiq do
    begin
      sh 'sidekiqctl stop tmp/sidekiq.pid'
    rescue StandardError
      ''
    end
  end
end
