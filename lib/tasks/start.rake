namespace :start do
  desc 'START etl mongo to mysql'
  task :etl do
    sh 'bundle exec kiba sources/ikea_fr/etl/mongo_mysql.etl'
  end

  desc 'START sidekiq and sk_front'
  task all: %i[sidekiq sk_front]

  desc 'START sidekiq debug'
  task :sk_debug do
    args = '-v -r ./config/environment.rb -L ./logs/sidekiq.log '
    args += '-q scraper -q main -q store -q parser '
    sh "sidekiq #{args}"
  end

  desc 'START sidekiq'
  task :sidekiq do
    args = '-v -r ./config/environment.rb -L ./logs/sidekiq.log '
    args += '-q scraper -q main -q store -q parser '
    args += '-P ./tmp/sidekiq.pid -d'
    sh "sidekiq #{args}"
  end

  desc 'START sidekiq front'
  task :sk_front do
    sh 'puma -d -p 2300'
  end
end
