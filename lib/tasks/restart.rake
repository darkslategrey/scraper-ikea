namespace :restart do

  desc 'reset and restart chouard_org'
  task :chouard do
    Rake::Task["reset:all"].invoke
    if Rake::Task["status:sk"].invoke == 'off'
      Rake::Task["start:sidekiq"].invoke
    else
      Rake::Task["restart:sidekiq"].invoke
    end
    sh './bin/cli.rb scrap chouard_org'
  end

  desc 'RESTART all'
  task :all do
    Rake::Task['stop:all'].invoke
    Rake::Task['start:all'].invoke
  end

  desc 'RESTART sidekiq'
  task :sidekiq do
    Rake::Task['stop:sidekiq'].invoke
    Rake::Task['start:sidekiq'].invoke
  end
end
