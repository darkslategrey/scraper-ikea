module Scraper
  module ChouardOrg
    class Blog
      include Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        links = @nokodoc.xpath('//a')
        urls  = []
        to_keep = []
        links.each do |link|
          href = link.attribute('href')
          next if href.blank?
          next if href.value.blank?
          next if href.value !~ /blog\/....\/..\/..\//
          to_keep << link.text
          urls << href.value
        end
        [{ articles: urls[0..39].uniq }, {data: to_keep.uniq} ]
      #   [{ }, {data: to_keep[0..2] }]
      rescue StandardError => e
        log.error(e.message)
        raise e
      end
    end
  end
end
