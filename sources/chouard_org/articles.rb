module Scraper
  module ChouardOrg
    class Articles
      include Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        title   = @nokodoc.css('.entry-title').text.strip
        content = @nokodoc.xpath('//div[contains(@class, "content")]')
        content = content.xpath('.//p').map(&:text)

        comments = @nokodoc.xpath('//div[@id="comments"]')
        comments = comments.xpath('.//li')
        stored_comments = []
        comments.each do |comment|
          author_date      = comment.css('.comment-author.vcard').text
          body             = comment.css('.comment-body').text.strip

          author, date, _r = author_date.strip.split('|').map(&:strip)
          stored_comments << { author: author, date: date, body: body }
        end
        [{}, { comments: stored_comments, title: title, content: content }]
      rescue StandardError => e
        log.error(e.message)
        raise e
      end
    end
  end
end
