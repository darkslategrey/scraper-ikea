# frozen_string_literal: true

Faraday::Middleware.register_middleware lbc_login_cookie: lambda {
  Scraper::LBCLoginCookie
}

module Scraper
  # transform / store lbc cookie to be reused by http client
  class LBCLoginCookie < Faraday::Middleware
    attr_accessor :cookie_frame

    COOKIE_KEYS = %w[cookieFrame hideCookieFrame pro_tdb_beta
                     layout s srt luat sli my_ads_resync
                     last_resync].freeze
    COOKIE_FILE = 'spec/lbc/cookies/minimal_cookie.txt'

    def call(env)
      @cookie_frame ||= 0
      if @cookie_frame == 4
        cookie_h = cookie_to_h(env[:request_headers]['Cookie'])
        IO.write(COOKIE_FILE, cookie_to_s(cookie_h))
      end
      @cookie_frame += 1
      @app.call(env).on_complete do |response_env|
        response_env
      end
    end

    def cookie_to_h(cookie)
      cookie = cookie.split(';').map do |i|
        i.split('=').map(&:strip)
      end
      Hash[cookie]
    end

    def cookie_to_s(cookie)
      cookie.reject do |k, _v|
        COOKIE_KEYS.index(k).nil?
      end.to_a.map do |i|
        i.join('=')
      end.join(';')
    end
  end
end
