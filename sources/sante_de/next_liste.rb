module Scraper
  module SanteDe
    class NextListe
      include Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        ets_urls  = []
        next_list_url = []
        to_keep = []

        item_xpath = '//div[contains(@class, "searchresult_list_item")]'
        items = @nokodoc.xpath(item_xpath).select { |i|
          i.text.strip.size != 0
        }
        items.each do |item|
          url_class = 'searchresult_list_item_address'
          ets_urls << item.xpath(".//div[@class='#{url_class}']//a").select { |url|
            url.attribute('href').value =~ /^index/
          }
        end
        ets_urls = ets_urls.flatten.map { |url|
          url.attribute('href').value.strip
        }
        urls = { ets: ets_urls }
        [ urls, {data: to_keep }]
      rescue StandardError => e
        log.error(e.message)
        raise e
      end

      def build_next_list_urls
        city   = get_city
        start  = @nokodoc.css('span#pflegeheim_counter_shown').first.text.to_i
        total  = @nokodoc.css('span#pflegeheim_counter_total').first.text.to_i
        urls   = []
        if start < total
          params = next_list_params(city, start)
          # start += 20
          urls << "/index.php?#{params.to_param}"
        end
        urls
      end

      def get_city
        uri = @nokodoc.css('.lastsearch_ph').attribute('href').value;
        uri = URI.parse(uri)
        uri = uri.query.split('&')
        uri = uri.map { |i|
          ii = i.split('=')
          if ii.size == 1
            [ii[0], nil]
          else
            i.split('=')
          end
        }
        uri = Hash[uri]
        uri['multi']
      end

      def next_list_params(city, start)
        {"module"=>"nursinghome",
         "action"=>"listajax",
         "city"=>nil,
         "multi"=>city,
         "hiddenCoords"=>nil,
         "zip"=>nil,
         "ambit"=>"10",
         "order"=>"asc",
         "extSearchAktualisierung"=>"unbegrenzt",
         "flagMapDragSearch"=>"0",
         "extSearchEntgelt"=>"0+-+200%2B+Euro",
         "type"=>"1",
         "focus"=>"0",
         "FLAG_JOIN"=>"AND",
         "extSearchSortierung"=>"distance",
         "extSearchSortierungSort"=>nil,
         "extSearchSortierungSelect"=>nil,
         "extSearchTBVersion"=>"2017",
         "start"=>start}
      end
    end
  end
end
