module Scraper
  module SanteDe
    class Ets
      include Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        urls  = []
        to_keep = {}
        contact_name = @nokodoc.xpath('//h1[@id="contact_name"]').text.strip
        address = @nokodoc.xpath('//p[@id="contact_street_streetnumber"]')
        address = address.text.strip

        contact = get_contact
        preise  = get_preise
        prices_infos = get_prices_infos
        boxes = get_boxes
        # binding.pry
        to_keep[:name]    = contact_name
        to_keep[:address] = address
        to_keep[:contact] = contact
        to_keep[:preise]  = preise
        to_keep[:prices_infos] = prices_infos
        to_keep[:boxes] = boxes
        [{}, {data: to_keep }]
      rescue StandardError => e
        log.error(e.message)
        raise e
      end

      def get_boxes
        boxes   = {}
        headers = @nokodoc.css('.box_head')[2..-1]
        headers.each do |header|
          id = header.attribute('id').value
          content_id = id.sub('boxhead', 'box')
          content    = @nokodoc.css("##{content_id}").first
          content    = content.text.strip.split("\n").map { |i|
            i.strip }.select { |i| not i.blank? }
          boxes[header.text.strip] = content
        end
        boxes
      end

      def get_prices_infos
        extracted_infos = []
        infos = @nokodoc.css('#box_more_info_preise')
        infos.children.each do |child|
          next if child.text.strip.blank?
          extracted_infos << child.text.strip
        end
        extracted_infos
      end

      def get_preise
        title = @nokodoc.xpath('//h2[@id="boxhead_header_Preise"]').text
        prices = @nokodoc.xpath('//table[@id="price_table"]')
        headers = prices.xpath('.//tr').first.xpath('.//th').map(&:text)
        headers = headers.map(&:strip)
        headers = headers.map { |h| h.gsub("\n", ' ').gsub(/  */, ' ').strip }
        sub_headers = prices.xpath('.//tr')[1].xpath('.//th').map(&:text)
        values = prices.xpath('.//tr')[2..-1].map { |i|
          i.xpath('.//td').map(&:text).map(&:strip)
        }
        { headers: headers, sub_headers: sub_headers, values: values }
      end

      def get_contact
        contact = @nokodoc.xpath('//table[@id="contact_data"]')
        contact = contact.text.strip.split("\n")
        contact = contact.map(&:strip).select { |i| not i.blank? }
        contact = contact.map { |j|
          if j =~ /web/i
            [j.split(':')[0], j.split(':')[1..2].join(':')]
          else
            j.split(':')
          end
        }.to_h
        contact = Hash contact
      end
    end
  end
end
