# frozen_string_literal: true

require_relative '../../config/environment'
require 'pry'

module Scraper
  module IkeaFr
    class Availability
      include ::Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        log.debug('Run extract')
        data = Hash.from_xml @nokodoc.to_s
        [{}, data]
      rescue StandardError => e
        log.error(e.message)
        raise e
      end
    end
  end
end
