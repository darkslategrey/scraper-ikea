module Scraper
  module IkeaFr
    class HrefMatcher
      def regex(node_set, regex)
        node_set.find_all { |node| node['href'] =~ /#{regex}/ }
      end
    end

    class Subcategs
      include Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        products_xpath = '.product-compact__spacer'
        # categs_xpath = './/a[regex(., "fr/fr/catalog/categories")]'
        products = @nokodoc.css('.product-compact__spacer')
        hrefs = []
        add_data = {
          title: @nokodoc.xpath('//h1[@class="range-page-title__title"]').first.text
        }
        i = 0
        products.each do |product|
          break if i > 2
          i += 1
          hrefs << product.xpath('.//a').first['href']
        end
        [{product: hrefs}, [add_data]]
      end
    end
  end
end
