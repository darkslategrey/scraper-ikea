# frozen_string_literal: true

require 'pry'

module Scraper
  module IkeaFr
    class HrefMatcher
      def regex(node_set, regex)
        node_set.find_all { |node| node['href'] =~ /#{regex}/ }
      end
    end

    class Category
      include Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        title = @nokodoc.css('.range-page-title__title').text
        subcategs_xpath = '//a[@class="range-catalog-list__link"]'
        subcategs = @nokodoc.xpath(subcategs_xpath)# , HrefMatcher.new)
        hrefs = []
        add_data = {}
        subcategs_titles = []
        i = 0
        subcategs.each do |sc|
          break if i > 2
          i += 1
          hrefs << sc['href']
          subcategs_titles << sc.xpath('.//span').text
        end
        data = { subcategs_titles: subcategs_titles, category_title: title }
        [{subcategs: hrefs}, [data]]
      rescue StandardError => e
        log.error(e.to_s)
        raise e
      end
    end
  end
end
