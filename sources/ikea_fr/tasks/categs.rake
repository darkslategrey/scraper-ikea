require_relative '../../../config/boot'

namespace :ikea do
  def extract_categs
    categs = MongoProduct.all.map { |p| p.data['categories'] }.sort.uniq
    categs = categs.sort { |k, v| k.size <=> v.size }
    top_categs = categs.map { |c| c[0] }.sort.uniq
    all_categs = {}
    top_categs.each do |top|

      sub_cats = categs.select { |c| c[0] == top }
      sub_cats_name = sub_cats.map { |c| c[1] }.sort.uniq

      h = {}
      sub_cats_name.each do |sub_cat_name|
        sub_sub_cats = sub_cats.select { |c| c[1] == sub_cat_name }
        sub_sub_cats_name = sub_sub_cats.map { |c| c[2] }.compact
        h[sub_cat_name] = sub_sub_cats_name
      end

      all_categs[top] = h
    end
    print_categs(all_categs, '')
  end

  def print_categs(categs, indent)
    if categs.class == Hash
      categs.each do |k, v|
        puts "#{indent}#{k}"
        print_categs(v, "#{indent}  ")
      end
    else
      puts "#{indent}#{categs.join('|')}" if not categs.size.zero?
    end
  end

  desc 'extract ikea catgory hierarchy'
  task :categs do
    extract_categs
  end
end
