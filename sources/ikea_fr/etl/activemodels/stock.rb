# == Schema Information
#
# Table name: stocks
#
#  id                 :integer          not null, primary key
#  product_id         :integer          not null
#  market_id          :integer
#  sold_out           :boolean
#  price              :decimal(15, 2)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  promo_price        :float(24)
#  variety            :string(255)
#  caliber            :string(255)
#  origin             :string(255)
#  chemical_treatment :string(255)
#  category           :string(255)
#

class Stock < ActiveRecord::Base
  belongs_to :market
  belongs_to :product
  # audited only: [:price], on: [:update]

  validates :price, presence: true,
                    numericality: { greater_than_or_equal_to: 0 }
end
