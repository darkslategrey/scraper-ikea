# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  tva              :decimal(5, 2)
#  weight           :decimal(10, )
#  unit             :integer
#  conditioning     :string(255)
#  specific_name    :string(255)
#  main_picture     :string(255)
#  second_picture   :string(255)
#  specific_picture :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#  category_1       :string(255)
#  category_2       :string(255)
#  category_3       :string(255)
#

class Product < ActiveRecord::Base
  has_many :stocks
  # has_many :promotions

  validates :name, presence: true

  scope :starts_with, ->(name) { where('name like ?', "%#{name}%") }

  def self.csv_columns
    %w(id name tva weight unit conditioning category_1 category_2 category_3
       specific_name main_picture second_picture specific_picture)
  end
end
