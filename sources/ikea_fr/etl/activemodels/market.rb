# == Schema Information
#
# Table name: markets
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  longitude          :string(255)
#  latitude           :string(255)
#  address            :string(255)
#  created_at         :datetime
#  updated_at         :datetime
#  address_1          :string(255)
#  mangopay_user_id   :string(255)
#  mangopay_wallet_id :string(255)
#
require 'active_record'

class Market < ActiveRecord::Base
  attr_accessor :address_1
  attr_accessor :address_2
  attr_accessor :zip
  attr_accessor :city

  # attr_accessor for self.closest_from method
  attr_accessor :distance_from
  attr_accessor :duration_from

  has_many :stocks
  has_many :products, through: :stocks
  has_many :promotions
  has_many :courseurs, -> { distinct }, class_name: 'User'

  scope :active, lambda {
    where.not(mangopay_user_id: nil, mangopay_wallet_id: nil)
  }

  validates :name, presence: true
end
