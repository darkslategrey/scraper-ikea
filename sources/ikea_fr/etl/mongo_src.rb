# frozen_string_literal: true

require_relative '../../../config/environment'
require_relative '../../../lib/scraper/utils'

class MongoProduct
  include Mongoid::Document
  store_in collection: 'product'

  field :urls,    default: {}
  field :data,    default: {}
  field :outifle, default: ''
  field :ids,     default: {}
end

class MongoAvailability
  include Mongoid::Document
  store_in collection: 'availability'

  field :data,    default: {}
  field :outifle, default: ''
end

class MongoSrc

  def initialize; end

  def each
    MongoProduct.each do |prod|
      yield prod
    end
  end
end
