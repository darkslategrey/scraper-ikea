# frozen_string_literal: true
require 'active_record'
require 'mysql2'

require_relative './activemodels/product'
require_relative './activemodels/stock'
require_relative './activemodels/market'

class MySQLDest

  def initialize
    ActiveRecord::Base.establish_connection(ENV['MYSQL_URI'])
    @market = create_market
  end

  def create_market
    params = {}
    market = Market.find_by_name('ROSNY')
    raise if market.blank?

    params[:address] = '164 avenue de la plaine de France 95950 GONESSE'
    params[:name]    = 'IKEA Roissy Paris Nord 2'
    params[:longitude] = 2.494173
    params[:latitude]  = 48.975402
    market.update_attributes(params)
    market
  rescue
    Market.find_by_name('IKEA Roissy Paris Nord 2')
  end

  def write(row)
    price   = row.delete(:price)
    product = Product.create(row)
    stock   = Stock.create(product_id: product.id,
                           market_id:  @market.id,
                           price: price,
                           sold_out: 0)
    puts "product <#{product.name}> created"
  end

  def close
    ActiveRecord::Base.connection.close
  end
end
