# frozen_string_literal: true

class Transform1
  def initialize(modulo_value)
    @modulo_value = modulo_value
  end

  def process(row)
    (row[:index] % @modulo_value).zero? ? row : nil
  end
end
