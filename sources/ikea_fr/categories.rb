# frozen_string_literal: true

require_relative '../../config/environment'
require 'pry'

module Scraper
  module IkeaFr
    class HrefMatcher
      def regex(node_set, regex)
        node_set.find_all { |node| node['href'] =~ /#{regex}/ }
      end
    end

    class Categories
      include ::Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        categs_xpath = '//a[@class="range-catalog-list__link"]'
        categs = @nokodoc.xpath(categs_xpath, HrefMatcher.new)
        limit = 2 #  ENV.fetch('IKEA_CAT_LIMIT', 0).to_i
        hrefs_categs = []
        i = 0
        log.debug("total categs <#{categs.count}>")
        categs.each do |categ|
          break if not limit.zero? and i > limit
          hrefs_categs << categ.attribute('href').value
          i += 1
        end
        hrefs_categs.sort!.uniq!
        log.debug("<#{hrefs_categs.size}> categories was extracted")
        [{ category: hrefs_categs }, []]
      rescue StandardError => e
        log.error(e.to_s)
        raise e
      end
    end
  end
end
