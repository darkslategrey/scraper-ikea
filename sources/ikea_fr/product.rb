# frozen_string_literal: true

require 'pry'
require 'rdf/microdata'

module Scraper
  module IkeaFr
    class HrefMatcher
      def regex(node_set, regex)
        node_set.find_all { |node| node['href'] =~ /#{regex}/ }
      end
    end

    class Product
      include Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        urls = {}
        res = {}
        RDF::Microdata::Reader.new(StringIO.new(@nokodoc.to_s)) do |reader|
          graph = RDF::Graph.new
          graph << reader
          graph.each do |g|
            h = g.to_h
            v = ''
            case h[:object].class.name
            when 'RDF::Literal::HTML'
              v = h[:object].text.strip
            when 'RDF::URI'
              v = h[:object].value
            when 'RDF::Literal'
              v = h[:object].value.strip
            else
              next
            end
            k = h[:predicate].value.split('/')[-1]
            res[k] = v
          end
          res.deep_symbolize_keys!
        end
        [urls, res]
      rescue StandardError => e
        log.error(e.message)
        raise e
      end
    end
  end
end
