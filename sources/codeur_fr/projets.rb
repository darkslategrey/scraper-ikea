# frozen_string_literal: true

require_relative '../../config/environment'
require 'pry'

module Scraper
  module CodeurFr
    class DivMatcher
      def regex(node_set, regex)
        node_set.find_all { |node| node['id'] =~ /#{regex}/ }
      end
    end

    class Projets
      include ::Scraper::BLogger

      def initialize(nokodoc)
        @nokodoc = nokodoc
      end

      def run
        projets_xpath = './/div[regex(., "project-")]'
        # binding.pry
        projets = @nokodoc.xpath(projets_xpath, DivMatcher.new)
        hrefs = projets_hrefs_ex(projets)
        log.debug("<#{hrefs.size}> projets was extracted")
        hrefs = { projet: hrefs }
        # hrefs[:page_suivante: page_suivante_ex }
        [hrefs, []]
      rescue StandarError => e
        log.error(e.to_s)
        raise e
      end

      def projets_hrefs_ex(projets)
        limit  = ENV['CODEUR_PROJ_LIMITE'].to_i || 5
        i      = 0
        projets_hrefs = []
        projets.each do |projet|
          break if i > limit
          projets_hrefs << projet.xpath('.//a').attribute('href').value
          i += 1
        end
        projets_hrefs.sort.uniq
      rescue StandarError => e
        raise e
      end
    end
  end
end
