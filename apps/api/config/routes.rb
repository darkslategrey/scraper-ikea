# frozen_string_literal: true

# Configure your routes here
# See: http://hanamirb.org/guides/routing/overview/
#
# Example:
get '/', to: ->(_env) { [200, {}, ['Hello from Hanami!']] }
