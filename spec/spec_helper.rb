# frozen_string_literal: true

# Require this file for unit tests
ENV['HANAMI_ENV'] ||= 'test'

require_relative '../config/environment'
require 'minitest/autorun'
require 'minitest/reporters'

# Minitest::Reporters.use! [Minitest::Reporters::MeanTimeReporter.new]
# Minitest::Reporters.use! [Minitest::Reporters::ProgressReporter.new]
# Minitest::Reporters.use! [Minitest::Reporters::HtmlReporter.new]
Hanami.boot
