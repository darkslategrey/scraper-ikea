# frozen_string_literal: true

require 'spec_helper'

describe Scraper::Main do
  it 'should_init' do
    Scraper::Main.config.sources.first.name.wont_be_nil
  end
end
