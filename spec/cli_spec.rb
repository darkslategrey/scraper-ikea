require 'spec_helper'

describe 'cli' do

  it 'ls' do
    `./bin/cli.rb ls`.wont_be_nil
  end

  before { FileUtils.rm_r Dir['sources/test_fr*'] }
  after  { FileUtils.rm_r Dir['sources/test_fr*',
                              'spec/fixtures/test_fr',
                              'spec/sources/test_fr*'] }

  describe 'generate' do
    it 'generate_source_script' do
      File.exist?('sources/test_fr/blog.rb').must_equal false
      `./bin/cli.rb generate script test_fr:blog`
      File.exist?('sources/test_fr/blog.rb').must_equal true
    end

    it 'generate_source' do
      File.exist?('sources/test_fr.yml').must_equal false
      `./bin/cli.rb generate source test_fr blog`
      File.exist?('sources/test_fr.yml').must_equal true
      File.exist?('spec/sources/test_fr_spec.rb').must_equal true
    end
  end
end
