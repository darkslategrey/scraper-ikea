require_relative '../../spec_helper'

describe Scraper::Input do
  subject { Scraper::Input }

  let(:params) { {csv: { a: 1, b: 2 }, src_id: 1 } }

  it 'save' do
    input = subject.new(params)
    input.save.wont_be_nil
    input.src_id.must_equal 1
  end

  let(:data)  { { result: { one: 1, two: 2 } } }
  let(:input) { subject.new(params) }

  it 'add_data' do
    input.add_data(data, :a_parser).must_equal true
    input.reload
    input.attributes.keys.include? :a_parser
  end
end
