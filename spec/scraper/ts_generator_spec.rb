# frozen_string_literal: true

require 'spec_helper'

describe Scraper::TSGenerator do
  subject { Scraper::TSGenerator }

  it 'can_be_instantiated' do
    subject.new.wont_be_nil
  end

  it 'can_give_next_ts' do
    subject.new.next.class.must_equal Time
  end
end
