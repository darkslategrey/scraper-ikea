# frozen_string_literal: true

require 'spec_helper'
require 'sidekiq/testing'
require 'webmock/minitest'

describe Scraper::ParseWorker do
  subject { Scraper::ParseWorker }
  before do
    stub_request(:any, /.*testdomain.*/) { 'bla' }
    @scraper_input = Scraper::Input.create({src_id: 1, _id: '123'})
    module Scraper
      module TestdomainFr
        class B
          def initialize(nokodoc); end
          def run; end
        end
        class Parser3 < B; end
        class Parser2 < B
          def run
            [{ parser3: ['a/b', '/'] }, { data: %w[data1 data2] }]
          end
        end
      end
    end
  end
  after   {
    if File.exist? ENV['HTMLFILES_DIR']
      FileUtils.rm_r ENV['HTMLFILES_DIR']
    end
    Scraper::Input.delete_all
  }
  let(:outfile) do
    f = "#{ENV['HTMLFILES_DIR']}/#{src_conf.name}"
    "#{f}/1/projets_5a5881315daecb22840e0c46425c516d.html"
  end
  
  let(:parse_msg) do
    {
      source: Scraper::Utils.new.os_to_h(src_conf),
      parser: 'parser2',
      input: { src_id: 1, _id: @scraper_input.id.to_s },
      outfile: outfile
    }
  end
  let(:src_conf) do
    Scraper::Main.config.sources.select do |src|
      src.name == :testdomain_fr
    end.first.loaded
  end
  let(:msg) { Scraper::Utils.new.open_struct(parse_msg) }
  describe 'compute_ids_only_once' do
    let(:parser_urls) {
      { parser_1: [{url: 'url1', id: 123}, {url: 'url2', id: 345}] }
    }
    it 'return_ids_when_do_store' do
      subject.new.do_store(parser_urls,
                           { data: { a1: 1, b2: 2 }}, msg).wont_be_nil
    end
  end

  describe 'generic_test' do
    let(:parse_result) do
      [{ parser2: ['a/b'] }, 'parser1', nil]
    end

    before do
      Scraper::Input.delete_all
      Sidekiq::Testing.inline!
      Scraper::Input.create({ src_id: 1, _id: 1 })
    end

    it 'perfom' do
      IO.stub :read, '<html>bla</html>' do
        subject.perform_async(parse_msg).wont_be_nil
      end
    end


    let(:mocked_parser) { Minitest::Mock.new }
    before do
      mocked_parser.expect :run,
                           [{ parser3: ['a/b'] },
                            { data: %w[data1 data2]} ]
    end
    it 'should_do_parse' do
      ::Scraper::TestdomainFr::Parser2.stub :new, mocked_parser do
        IO.stub :read, '<html>bla</html>' do
          subject.new.do_parse(msg)
        end
      end
      mocked_parser.verify.must_equal true
    end
    let(:data)         { { data: %w[data1 data2]} }

    let(:_id)      { '901d7239bff6b6c7a14d8b61f4e50cce' }
    let(:urls_ids) { {"parser1"=>[ { url: 'a/b', id: _id } ]} }

    it 'should_do_scrap' do
      subject.new.do_scrap(urls_ids,
                           msg.source, msg.input).must_equal ['a/b']
    end
  end
end
