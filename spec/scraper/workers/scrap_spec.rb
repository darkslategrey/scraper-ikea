require 'spec_helper'
require 'sidekiq/testing'

require_relative '../../../lib/scraper/workers/scrap'

describe Scraper::ScrapWorker do
  include Scraper::BLogger

  subject { Scraper::ScrapWorker }

  let(:src_conf) do
    Scraper::Main.config.sources.select do |src|
      src.name == :testdomain_fr
    end.first.loaded
  end
  let(:url)     { 'a/b' }
  let(:parser)  { 'parser1' }
  let(:input) {
    Scraper::Utils.new.open_struct({ src_id: 1 }) }
  let(:outfile) {
    Scraper::Utils.new.make_filename(parser, 134, msg_os, input.src_id)
  }
  let(:msg_os) {
    Scraper::Utils.new.open_struct(
      {
        urls:    { parser => [url] },
        source:  src_conf
      })
  }

  let(:scrap_msg) {
    { outfile: outfile, parser: parser,
      url: url, input: Scraper::Utils.new.os_to_h(input),
      source: Scraper::Utils.new.os_to_h(src_conf) }
  }

  let(:spider) { Minitest::Mock.new }

  before do
    i = Scraper::Input.create(input.attributes)
    input._id = i.id.to_s
    spider.expect :go, 'scraped', ['a/b']
    Sidekiq::Testing.inline!
    # log.level = :debug
  end

  it 'should_call_spider' do
    Scraper::Spider.stub :new, spider do
      subject.perform_async(scrap_msg)
    end
    spider.verify.must_equal true
  end
end
