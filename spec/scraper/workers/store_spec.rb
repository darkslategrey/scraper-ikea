# frozen_string_literal: true

require 'spec_helper'

describe Scraper::StoreWorker do
  subject { Scraper::StoreWorker }

  let(:parser) { 'parser1' }
  let(:db)     { Mongo::Client.new(ENV['MONGODB_URI']).database }
  let(:parser_coll)   { db[parser.to_sym] }
  let(:outfile) { 'product_03a4da2d74a8b9c27958ffee69c67d6d.html' }
  let(:input)   { { src_id: 2, _id: scraper_input.id.to_s }.stringify_keys }

  let(:msg) do
    Scraper::Utils.new.open_struct(urls: ['a/b'],
                                   outfile: outfile,
                                   data: { a: 1, b: 2 },
                                   ids: ['akdkf132'],
                                   input: input,
                                   parser: parser)
  end
  let(:id) { '03a4da2d74a8b9c27958ffee69c67d6d' }

  before do
    Scraper::Input.delete_all
    parser_coll.drop
    
  end
  let(:scraper_input) { Scraper::Input.create(src_id: 2) }
  
  after  { parser_coll.drop }

  it 'can_be_instantiated' do
    subject.new.wont_be_nil
  end

  let(:data) { { d1: :v1, d2: :v2 } }
  let(:os_input) { Scraper::Utils.new.open_struct(input) }
  let(:h_input) { Scraper::Input.find(os_input._id).attributes }
  it 'store_input' do
    subject.new.store_input(os_input, data, parser)
    h_input['parser1'].first.deep_symbolize_keys.must_equal data
  end
  describe 'store new object' do
    it 'should_store_new_object_with_defined_id' do
      parser_coll.count.must_equal 0
      subject.new.do_store(msg)
      parser_coll.count.must_equal 1
    end
  end

  describe 'update existing object' do
    before { subject.new.do_store(msg) }
    it 'should_update_object' do
      parser_coll.count.must_equal 1
      data = parser_coll.find({}).first['data'].deep_symbolize_keys
      data.must_equal msg.data.to_h
      msg.data = { d: 3, e: 4 }
      subject.new.do_store(msg)
      parser_coll.count.must_equal 1
      data = parser_coll.find({}).first['data'].deep_symbolize_keys
      data.must_equal msg.data
    end
  end
end
