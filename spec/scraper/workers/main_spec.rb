# frozen_string_literal: true

require 'spec_helper'
require 'webmock/minitest'
require 'sidekiq/testing'

describe Scraper::MainWorker do
  subject { Scraper::MainWorker }
  before  do
    stub_request(:any, /.*www.testdomain.fr.*/) { 'bla' }
    FileUtils.rm_r ENV['HTMLFILES_DIR'] if File.exist? ENV['HTMLFILES_DIR']
    Sidekiq::Testing.inline!
    FileUtils.mkdir_p filename.split('/')[0..-2].join('/')
    FileUtils.touch  filename
  end

  after   do
    if File.exist? ENV['HTMLFILES_DIR']
      FileUtils.rm_r ENV['HTMLFILES_DIR']
    end
    File.delete('a_filename') if File.exist? 'a_filename'
  end

  let(:src_conf) do
    Scraper::Main.config.sources.select do |src|
      src.name == :testdomain_fr
    end.first.loaded
  end

  let(:filename) do
    f = "#{ENV['HTMLFILES_DIR']}/#{src_conf.name}"
    "#{f}/#{input.id}/parser1_123.html"
  end

  let(:input) {
    Scraper::CSVInput.new({ id: 1, src_id: 1, _id: 1})
  }
  it 'make_filename' do
    Scraper::Utils.new.make_filename('parser1',
                                     123,
                                     os_msg,
                                     input.src_id).must_equal filename
  end

  it 'default_inputs' do
    subject.new.default_inputs.first.to_h.keys.must_equal [:_id,
                                                           :src_id,
                                                           :u_at,
                                                           :c_at]
  end

  let(:now)           { Time.now }
  let(:hashed_msg)    { Scraper::Utils.new.os_to_h(src_conf) }

  let(:params) do
    {
      urls:    { 'parser1' => ['a/b'] },
      source:  Scraper::Utils.new.os_to_h(src_conf)
    }
  end
  let(:os_msg) { Scraper::Utils.new.open_struct(params) }

  describe 'when outfile does not exists' do
    before do
      scraper.expect :perform, true, [Object]
      scraper.expect :jid=, true, [String]
    end
    let(:scraper) { Minitest::Mock.new }

    it 'should_call_scrap_worker' do
      Scraper::ScrapWorker.stub :new, scraper do
        subject.perform_async(params)
      end
      scraper.verify
    end
  end

  describe 'when outfile does exists' do
    before do
      parser.expect :perform, true, [Object]
      parser.expect :jid=, true, [String]
    end
    let(:parser) { Minitest::Mock.new }
    it 'should_call_parse_worker' do
      Scraper::ParseWorker.stub :new, parser do
        subject.perform_async(params)
      end
      parser.verify
    end
  end
end
