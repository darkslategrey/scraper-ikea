require 'spec_helper'

describe Scraper::Spider do

  subject { Scraper::Spider }

  let(:scraper_input) { Scraper::Input.create(input) }

  let(:params) {
    conf = Scraper::Utils.new.conf_by(:testdomain_fr)
    conf = Scraper::Utils.new.open_struct(conf)
    conf = conf.loaded
    input['_id'] = scraper_input.id.to_s
    conf.input = input
    conf.current = conf.send(:start)
    conf
  }

  let(:no_host) { OpenStruct.new }
  it 'should_raise_when_no_host' do
    proc {
      Scraper::BLogger.stub :error, true do
        subject.new(no_host)
      end
    }.must_raise
  end

  it 'should_use_headers' do
    s = subject.new(params)
    s.headers[:'user-agent'].must_match(/mozilla/i)
    s.http_method.must_equal :post
  end

  let(:request) { {"q":"<%= term_1 %> <%= term_2 %>"} }
  let(:input)   do
    {
      'csv_data' => { term_1: 't1', term_2: 't2' },
      'src_id'   => 1
    }
  end

  it 'resolve_request' do
    subject.new(params).request.must_equal({ q: 't1, t2' })
  end

  it 'should_set_current_parser' do
    subject.new(params).current.wont_be_nil
  end
end
