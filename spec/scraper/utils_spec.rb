# frozen_string_literal: true

require 'spec_helper'

describe Scraper::Utils do
  subject { Scraper::Utils }

  it 'can_be_instantiated' do
    subject.new.wont_be_nil
  end

  let(:openstruct) { attendee }
  let(:hashed)     { { a: 1, b: [{ c: 2, d: 3 }, { d: 4, e: 5 }], f: 6 } }

  let(:header_keys) { [ :user_agent, :method ] }

  it 'conf_by_name' do
    subject.new.conf_by(:testdomain_fr).loaded.headers[:'user-agent'].wont_be_nil
  end

  let(:from) {
    'htmlfiles/1234/codeur_fr/projets_1e3a92d0411dea4bf1d80a45da9e8b75.html'
  }
  let(:to) { '1e3a92d0411dea4bf1d80a45da9e8b75' }
  it 'file_to_id' do
    subject.new.file_to_id(from).must_equal to
  end
  it 'os_to_h' do
    subject.new.os_to_h(openstruct).must_equal hashed
  end

  let(:src_conf) do
    Scraper::Main.config.sources.select do |src|
      src.name == :testdomain_fr
    end.first.loaded
  end


  let(:params) do
    {
      urls:    urls,
      source:  src_conf
    }
  end
  let(:msg) { Scraper::Utils.new.open_struct(params) }
  let(:input) { Scraper::Utils.new.open_struct({ src_id: 1 }) }

  let(:id_attendee)   { '901d7239bff6b6c7a14d8b61f4e50cce' }
  let(:urls_attendee) { {"parser1"=>[ { url: 'a/b', id: id_attendee} ]} }
  let(:urls) { { 'parser1' => ['a/b'] } }

  it 'urls_to_ids' do
    subject.new.urls_to_ids(urls, input).must_equal urls_attendee
  end

  let(:make_attendee) { '1fb60ec928bd4f8bab8843365b3b3b85' }
  it 'make_filename' do
    subject.new.make_filename('parser_1', make_attendee, msg,
                              input.src_id).must_match(/#{make_attendee}/)
  end


  let(:loaded_inputs) {
    subject.new.load_inputs(inputs_params) }

  let(:loaded_attendee) {
    [{:csv_data=>{"term_1"=>"alan", "term_2"=>"turing"}, :src_id=>"1"},
     {:csv_data=>{"term_1"=>"gerard", "term_2"=>"lenormand"}, :src_id=>"2"}]
  }

  it 'load_inputs' do
    loaded_inputs.map(&:to_h).must_equal loaded_attendee
  end
  # before { Scraper::Input.delete_all }

  it 'store_inputs' do
    Scraper::Input.delete_all
    first = Scraper::Utils.new.store_inputs(loaded_inputs).first
    first[:_id].must_match(/[a-z0-9]{20,}/)
    first[:src_id].wont_be_nil
    Scraper::Input.count.must_equal 2
    Scraper::Input.first.csv_data.wont_be_nil
  end

  let(:headers)    { 'id;header_1;header_2' }
  let(:csv_data)   { "1;alan;turing\n2:gerard;lenormand" }
  let(:inputs_attendee)   {
    Set.new([Set.new(['gerard', "2", 'lenormand']),
             Set.new(['alan', "1", 'turing'])]) }

  let(:inputs_params) {
    { inputs: 'sources/testdomain_fr/inputs.csv' } }

  let(:erb_params) { '<%= h_1 %> <%= h_2 %>' }
  let(:erb_params2) { '<%= term_1 %> <%= term_2 %>' }
  let(:input_1) { Scraper::CSVInput.new(h_1: 'alan', id: 1, h_2: 'turing') }
  let(:input_2) {
    Scraper::CSVInput.new(h_1: 'gerard', id: '2',
                          h_2: 'lenormand') }
  let(:input_3) {
    Scraper::CSVInput.new({"term_1"=>"alan", "term_2"=>"turing"})
  }

  it 'build_erb_param' do
    subject.new.build_erb_param(erb_params, input_1).must_equal 'alan turing'
    subject.new.build_erb_param(erb_params2, input_3).must_equal 'alan turing'
  end

  describe 'yaml' do
    it 'should_load_yamls' do
      subject.new.load_yamls.first.wont_be_nil
    end
  end

  let(:obj) do
    { a: 1, b: [{ c: 2, d: 3 }, { d: 4, e: 5 }], f: 6 }
  end

  let(:attendee) do
    args = {
      a: 1, b: [OpenStruct.new(c: 2, d: 3), OpenStruct.new(d: 4, e: 5)], f: 6
    }
    OpenStruct.new args
  end
  it 'must_deep_openstruct_structure' do
    subject.new.open_struct(obj).must_equal attendee
  end
end
