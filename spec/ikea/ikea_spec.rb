# frozen_string_literal: true

require 'spec_helper'

require 'nokogiri'

require_relative '../../sources/ikea_fr/categories'
require_relative '../../sources/ikea_fr/category'
require_relative '../../sources/ikea_fr/product'

describe Scraper::IkeaFr do
  after do
    FileUtils.rm_r ENV['HTMLFILES_DIR'] if File.exist? ENV['HTMLFILES_DIR']
  end

  describe 'live tests' do
    let(:host) { 'http://www.ikea.com' }
    let(:http) do
      Faraday.new(url: host) do |faraday|
        faraday.request  :url_encoded
        faraday.request  :multipart
        faraday.use FaradayMiddleware::FollowRedirects, limit: 15
        # standards_compliant: true,
        # callback: lambda { |resp_env, new_req_env|
        # puts "REDIRECT COOKIE <#{resp_env[:request_headers]['Cookie']}"
        # puts 'une redirect'
        # }
        # faraday.response :logger
        faraday.use :repeater, retries: 3, mode: :rapid
        faraday.use :cookie_jar
        faraday.use :gzip
        # faraday.use :lbc_login_cookie
        faraday.adapter Faraday.default_adapter
        # faraday.adapter :excon
      end
    end
    let(:ua) { 'Mozilla/5.0 (X11; Linux x86_64)
               AppleWebKit/537.36 (KHTML, like Gecko)
               Chrome/63.0.3239.108 Safari/537.36' }
    let(:headers) do
      {
        'Connection': 'keep-alive',
       'Pragma': 'no-cache',
       'Cache-Control': 'no-cache',
       'Upgrade-Insecure-Requests': '1',
       'User-Agent': ua,
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
       'Accept-Encoding': 'gzip, deflate',
       'Accept-Language': 'en-US,en;q=0.9,fr;q=0.8'
      }
    end
    it 'should_get_categories_page' do
      skip ''
      http.headers.merge! headers
      response = http.get('/ms/fr_FR/produits.html')
      IO.write('categories.html', response.body)
    end
  end
  describe 'Categories' do
    let(:file) { 'spec/fixtures/ikea/categories.html' }
    let(:nokodoc) do
      Nokogiri::HTML(IO.read(file))
    end
    subject { Scraper::IkeaFr::Categories.new(nokodoc) }

    it 'should_return_all_categories_url' do
      subject.run[0][:category].count.must_equal 213
    end
  end

  describe 'Category2' do
    let(:htmlfiles) { 'spec/fixtures/ikea/category.html' }
    let(:nokodoc) do
      Nokogiri::HTML(IO.read(htmlfiles))
    end
    let(:urls) { 0 }

    subject  { Scraper::IkeaFr::Category.new(nokodoc) }

    it 'should_return_scraper' do
      subject.run.must_be_nil
    end

    it 'should_return_products_url' do
      subject.run[urls][:product].count.must_be :>, 0
    end
  end


  describe 'Category' do
    let(:htmlfiles1) { Dir["#{ENV['HTMLFILES_DIR']}/categs_*.html"] }
    let(:htmlfiles2) { 'spec/fixtures/ikea/category.html' }
    let(:nokodoc) do
      Nokogiri::HTML(IO.read(htmlfiles2))
    end
    let(:urls) { 0 }

    subject      { Scraper::IkeaFr::Category.new(nokodoc) }
    let(:object) { Scraper::IkeaFr::Category }

    it 'should_return_scraper' do
      htmlfiles1.each do |file|
        nokodoc = Nokogiri::HTML(IO.read(file))
        object.new(nokodoc).run[2].wont_be_nil
      end
    end

    it 'should_return_products_url' do
      subject.run[urls][:product].count.must_be :>, 0
    end
  end

  describe 'Product 2' do
    let(:nokodoc) do
      Nokogiri::HTML(IO.read('spec/fixtures/ikea/product.html'))
    end

    subject { Scraper::IkeaFr::Product.new(nokodoc) }

    it 'run_2' do
      subject.run.must_be_nil
    end
  end

  describe 'Product' do
    let(:nokodoc) do
      Nokogiri::HTML(IO.read('spec/fixtures/ikea/product.html'))
    end
    let(:breadcrumbs) do
      ['Bureau ', 'Bureaux et supports pour ordinateur', 'Bureaux pour ordinateurs fixes']
    end

    subject { Scraper::IkeaFr::Product.new(nokodoc) }

    it 'should_return_breadcrumbs' do
      subject.breadcrumbs.must_equal breadcrumbs
    end

    let(:product_infos) do
      %w[items catEntryId attributes partNumber].map(&:to_sym)
    end

    it 'should_return_product_info' do
      subject.product_info.deep_symbolize_keys.keys.must_equal product_infos
    end

    let(:correct_hash) { product_infos << :categories }

    it 'should_return_correct_hash' do
      product_data = subject.run[1].deep_symbolize_keys
      product_data.keys.must_equal correct_hash
    end

    let(:stock_search) do
      %w[
        productId storeId langId partNumber
        shortURL localStores
      ]
    end
    let(:urls) do
      ['/fr/fr/iows/catalog/availability/30339735/']
    end
    it 'should_return_search_from_data' do
      subject.run[0][:availability].must_equal urls
    end
  end
end
