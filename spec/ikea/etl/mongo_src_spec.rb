# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../sources/ikea_fr/etl/mongo_src'

describe MongoSrc do
  subject { MongoSrc }

  describe 'from mongodb' do

    it 'can_be_instantiated' do
      subject.new.wont_be_nil
    end
  end
end

