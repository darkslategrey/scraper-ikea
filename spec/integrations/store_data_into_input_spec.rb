require 'spec_helper'
require 'sidekiq/testing'
require 'webmock/minitest'

describe 'store' do
  it 'store' do
    
  end
end
describe 'source can be configured with inputs to post' do
  describe 'when inputs param is present the cycle is run for each input' do
    before {
      Sidekiq::Testing.inline!
      stub_request(:any, /.*www.testdomain.fr.*/) { 'bla' }
    }
    after  { Sidekiq::Testing.fake! }

    let(:conf_src) do
      Scraper::Main.config.sources.select do |src|
        src.name == :testdomain_fr
      end.first.loaded
    end

    let(:params) {
      {
        urls:   Scraper::Utils.new.os_to_h(conf_src.start.urls),
        source: Scraper::Utils.new.os_to_h(conf_src)
      }
    }
    it 'should_run_main_worker' do
      Scraper::MainWorker.perform_async(params).wont_be_nil
    end
  end
end
