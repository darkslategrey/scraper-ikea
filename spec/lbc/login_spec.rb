# frozen_string_literal: true

require 'spec_helper'
require_relative './lbc_http'

describe 'go_login_into_lbc' do
  include LBCHttp

  let(:host) { 'https://compteperso.leboncoin.fr' }

  let(:params) do
    {
      st_username: 'lucien.farstein@gmail.com',
      st_passwd: 'toto555500'
    }
  end
  let(:headers) { { mimeType: 'application/x-www-form-urlencoded' } }
  let(:url)     { '/store/verify_login/0' }

  it 'should_get_dashboard' do
    skip ''
    response = http(host).post(url, params)
    response.status.must_equal 200
    response.body.must_match 'Mon tableau de bord'
  end
end
