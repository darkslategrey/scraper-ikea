# frozen_string_literal: true

require 'spec_helper'
require_relative './lbc_http'

describe 'get index page' do
  include LBCHttp

  let(:host)   { 'https://www.leboncoin.fr' }
  let(:client) { http(host) }

  let(:cookie_file) { 'spec/lbc/cookies/minimal_cookie.txt' }
  let(:cookie)      { { Cookie: IO.read(cookie_file) } }

  describe 'when connected' do
    before { client.headers.merge!(cookie) }

    it 'should_get_the_connected_index_page' do
      skip ''
      response = client.get('/')
      response.status.must_equal 200
      response.body.must_match(/lucien/i)
    end
  end

  describe 'when not connected' do
    it 'should_get_the_standard_index' do
      skip ''
      response = client.get('/')
      response.status.must_equal 200
      response.body.wont_match(%r{/lucien/i})
    end
  end
end
