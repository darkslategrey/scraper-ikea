# frozen_string_literal: true

require_relative '../../sources/lbc_fr/middleware'

module LBCHttp
  def headers
    {
      'Content-Type' =>  'multipart/form-data; boundary=----WebKitFormBoundaryaY6FcbNpd1e4m7vI'
    }
  end

  def http(host)
    WebMock.allow_net_connect!
    Faraday.new(url: host) do |faraday|
      faraday.request  :url_encoded
      faraday.request  :multipart
      faraday.use FaradayMiddleware::FollowRedirects, limit: 10,
                  standards_compliant: true,
                  callback: lambda { |resp_env, new_req_env|
        # puts "REDIRECT COOKIE <#{resp_env[:request_headers]['Cookie']}"
        # puts 'une redirect'
      }
      # faraday.response :logger
      faraday.use :repeater, retries: 3, mode: :rapid
      faraday.use :cookie_jar
      faraday.use :lbc_login_cookie
      faraday.adapter Faraday.default_adapter
      # faraday.adapter :excon
    end
  end
end
