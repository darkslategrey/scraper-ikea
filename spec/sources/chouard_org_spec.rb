require 'spec_helper'

describe Scraper::ChouardOrg::Blog do
  subject { Scraper::ChouardOrg::Blog.new(nokodoc) }

  let(:file)    { 'spec/fixtures/chouard_org/blog.html' }
  let(:nokodoc) { Nokogiri::HTML(IO.read(file)) }

  it 'should_return_something' do
    subject.run.first[:articles].size.must_be :>, 10
    subject.run.last[:data].size.must_be :>, 10
  end
end
