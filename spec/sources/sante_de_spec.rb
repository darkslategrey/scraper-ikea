require 'spec_helper'

describe Scraper::SanteDe do
  describe Scraper::SanteDe::Liste do
    let(:list_parse)   { Scraper::SanteDe::Liste.new(list_nokodoc) }
    let(:list_file)    { 'spec/fixtures/sante_de/liste.html' }
    let(:list_nokodoc) { Nokogiri::HTML(IO.read(list_file)) }

    it 'should_return_ets_and_next_list' do
      list_parse.run.first[:ets].size.must_be :>, 10
      list_parse.run.first[:next_liste].size.must_be :>, 0
    end
  end

  describe Scraper::SanteDe::Ets do
    let(:ets_parse)  { Scraper::SanteDe::Ets.new(ets_nokodoc) }
    let(:ets_file)   { 'spec/fixtures/sante_de/ets.html' }
    let(:ets_nokodoc) { Nokogiri::HTML(IO.read(ets_file)) }

    it 'should_return_ets_infos' do
      ets_parse.run.last[:data].size.must_be :>, 4
    end
  end
end
