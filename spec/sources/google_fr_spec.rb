require 'spec_helper'

describe Scraper::GoogleFr::Results do
  subject { Scraper::GoogleFr::Results.new(nokodoc) }

  let(:file)    { 'spec/fixtures/google_fr/results.html' }
  let(:nokodoc) { Nokogiri::HTML(IO.read(file)) }

  it 'should_return_something' do
    subject.run.map(&:keys).flatten.must_equal [:data]
  end
end
