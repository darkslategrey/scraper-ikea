#!/bin/bash

# trap TERM and change to QUIT
# trap 'echo killing $PID; kill -QUIT $PID' TERM

# program to run
ruby -r ./config/environment.rb lib/scraper/workers/parse.rb script &

# capture PID and wait
PID=$!
echo $PID > tmp/parser.pid
# wait
