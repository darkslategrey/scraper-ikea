#!/bin/bash

ruby -r ./config/environment.rb lib/scraper/workers/store.rb script &
PID=$!
echo $PID > tmp/store.pid
