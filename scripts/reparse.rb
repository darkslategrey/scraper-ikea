# frozen_string_literal: true

require 'redis'

Dir['htmlfiles/category*.html'].each do |category|
  Redis.new.publish 'parse',
                    '{"parser":"Category","outfile":"' + category + '"}'
end
